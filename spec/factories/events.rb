# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :event do
    title "MyString"
    body "MyText"
    event_date "2014-03-31"
    room "MyString"
    music "MyString"
    scripture "MyString"
    user_id 1
  end
end
