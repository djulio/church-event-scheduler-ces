class Users::RegistrationsController < Devise::RegistrationsController
  before_filter :check_permissions, :only => [:new, :create, :cancel]
  skip_before_filter :require_no_authentication

 # devise :database_authenticatable, :registerable,
  #       :recoverable, :rememberable, :trackable, :validatable

  def check_permissions
    authorize! :create, resource
  end
  def require_no_authentication


  end
end