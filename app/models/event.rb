class Event < ActiveRecord::Base
  attr_accessible :body, :start_at, :end_at, :music, :room_id, :scripture, :title, :category_ids, :user_id
  validates_presence_of :title, :body, :start_at, :end_at, :room_id ,:user_id, :categories
  belongs_to :user
  has_and_belongs_to_many :categories
  belongs_to :room
  has_event_calendar


  validate :start_before_end
  validate :uniqueness_of_date_range

  scope :published, lambda {where(['end_at <= ?', Date.today])}
  scope :by_user_id, lambda {|uid| where(:user_id => uid)}
  scope :by_category_id, lambda {|cid| joins(:categories).where(['categories.id =?',cid])}
  scope :past_event, lambda {where(['start_at >= ?', Date.today])}
end

def start_before_end
  if end_at <= start_at
    errors.add(:start_at, 'has to be before the end date')
    if start_at >= end_at
      errors.add(:start_at, 'has to be before the end date')

    end
  end

  def start_must_be_in_future

    if Date.today >= start_at
    errors.add(:start_at, "Date cannot be in past.")

    end
  end

  def uniqueness_of_date_range

    errors.add(:room_id, "Room is reserved. Please choose a different room.") unless Event.where("? == room_id AND ? >= start_at AND ? <= end_at", room_id, start_at, end_at).count == 0
    errors.add(:room_id, "Room is reserved. Please choose a different room.") unless Event.where("? == room_id AND ? >= start_at AND ? <= end_at", room_id, end_at, end_at).count == 0

  end

  def reserved
    @Event.new[:room_id]

  end

end



