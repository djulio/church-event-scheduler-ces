class Category < ActiveRecord::Base
  attr_accessible :eventType
  validates_presence_of :eventType
  has_and_belongs_to_many :events
end
