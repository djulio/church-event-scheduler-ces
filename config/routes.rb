CES::Application.routes.draw do
  resources :rooms


  resources :categories


  resources :events

  match '/calendar(/:year(/:month))' => 'calendar#index', :as => :calendar, :constraints => {:year => /\d{4}/, :month => /\d{1,2}/}


  authenticated :user do
    root :to => 'events#index'
  end
  root :to => "events#index"
  devise_for :users,  :controllers => { :registrations => "users/registrations" }
  resources :users

  #, :only => [:show, :index]
end