class CreateEvents < ActiveRecord::Migration
  def change
    create_table :events do |t|
      t.string :title
      t.text :body
      t.datetime :start_at
      t.datetime :end_at
      t.integer :room_id
      t.string :music
      t.string :scripture
      t.integer :user_id

      t.timestamps
    end
  end
end
